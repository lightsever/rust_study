use actix_web::{ middleware, web, App, HttpRequest, HttpServer,HttpResponse,Responder};

pub async fn index(req: HttpRequest) -> &'static str {
    println!("REQ: {:?}", req);
    return "Hello world Index!";
}
pub async fn greet(req: HttpRequest) -> impl Responder {
    let name = req.match_info().get("name").unwrap_or("World");
    return format!("Hello {}!", &name);
}