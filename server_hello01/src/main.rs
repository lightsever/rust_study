mod onget;

use std::{thread, time};

use actix_rt::System;
use actix_web::{ middleware, web, App, HttpRequest, HttpServer,HttpResponse,Responder};



//actix http Server线程
fn run_app() -> std::io::Result<()> {
    let mut sys = System::new("test");

    // srv is server controller type, `dev::Server`
    let srv = HttpServer::new(|| {
        App::new()
            // enable logger
            .wrap(middleware::Logger::default())
            .service(web::resource("/index.html").to(onget::index))
            .route("/", web::get().to(onget::greet))
    })
    .bind("127.0.0.1:8080")?
    .run();
    // run future
    return sys.block_on(srv);
}

fn main() {
    std::env::set_var("RUST_LOG", "actix_web=info,actix_server=trace");
    env_logger::init();

    println!("START SERVER");
    thread::spawn(move || {
        run_app().expect("error start http server");
    });

    //主循环
    loop
    {
        let mut line =String::new();
        let _ = std::io::stdin().read_line(&mut line);
        
        thread::sleep(time::Duration::from_secs(10));
    }

}


