@echo ====build wasm====
@wasm-pack build --target no-modules --dev

@echo ====copy pkg files====
@copy pkg\wasm.js wasm.js
@copy pkg\wasm_bg.wasm wasm_bg.wasm