"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
// declare module wasm_bindgen
// {
//     function greet(name: string): void;
// }
window.onload = () => __awaiter(void 0, void 0, void 0, function* () {
    var wasm = yield wasm_bindgen("./wasm_bg.wasm");
    //wasm_bindgen.greet("peter");
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBwLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFHQSw4QkFBOEI7QUFDOUIsSUFBSTtBQUNKLDBDQUEwQztBQUMxQyxJQUFJO0FBRUosTUFBTSxDQUFDLE1BQU0sR0FBRyxHQUFTLEVBQUU7SUFFdkIsSUFBSSxJQUFJLEdBQUcsTUFBTSxZQUFZLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztJQUVoRCw4QkFBOEI7QUFDbEMsQ0FBQyxDQUFBLENBQUEifQ==