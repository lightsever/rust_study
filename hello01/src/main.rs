use std::io::*;

fn main() {
    for x in 1..10 {
        for y in 1..10 {
            if y > x {
                break;
            }
            let s = format!("{} * {} = {} \t", x, y, x * y);
            stdout().write(s.as_bytes()).expect("error");
            //由于write定义在std::io::Write中，必须use 他
            //直接use std::io::*;一了百了
        }
        std::io::stdout().write(b"\n").expect("error");
        //上面有use，std::io::可以省略
    }

    let vec0 = Vec::new();
    println!("{} has length {} content `{:?}`", "vec0", vec0.len(), vec0);
 
    let mut vec1 = fill_vec(vec0);
    println!("{} has length {} content `{:?}`", "vec1", vec1.len(), vec1);

    vec1.push(88);
    println!("{} has length {} content `{:?}`", "vec1", vec1.len(), vec1);
}
fn fill_vec(vec: Vec<i32>) -> Vec<i32> {
    let mut vec =vec;
    vec.push(22);
    vec.push(44);
    vec.push(66);
    return vec;
}
