@echo ====build wasm====
@cd wasm
@wasm-pack build --target no-modules
@cd ..

@echo ====copy pkg files====
@copy wasm\pkg\wasm.js html\wasm.js
@copy wasm\pkg\wasm_bg.wasm html\wasm_bg.wasm
@pause