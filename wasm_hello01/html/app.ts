
//import * as wasm_bindgen from "./wasm";
declare function  wasm_bindgen (module_or_path: RequestInfo | BufferSource | WebAssembly.Module): Promise<any>;
declare module wasm_bindgen
{
    function greet(name: string): void;
}

window.onload = async () =>
{
    var wasm = await wasm_bindgen("./wasm_bg.wasm");
  
    wasm_bindgen.greet("peter");
}

